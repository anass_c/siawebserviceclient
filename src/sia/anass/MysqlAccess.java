package sia.anass;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class MysqlAccess {
	Connection connect = null;
	PreparedStatement preparedStatement;
	ResultSet resultSet;
	String query;

	public void DBConnect() throws Exception {
		try {
			// This will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			// Setup the connection with the DB
			connect = DriverManager.getConnection("jdbc:mysql://localhost/sia_projet1?", "anass",
					"123456");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public boolean isClientExist(String name) throws Exception {
		DBConnect();
		query = "select * from TClient where NomCli=?";
		preparedStatement = (PreparedStatement) connect.prepareStatement(query);

		preparedStatement.setString(1, name);
		resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {
			close();
			return true;
		}
		close();
		return false;
	}

	public boolean insertClient(String name) throws Exception {
		DBConnect();
		if (isClientExist(name)) {
			System.err.println("User already exist!!");
			return false;
		}
		query = "INSERT INTO sia_projet1.TClient (`NumCli`, `NomCli`) VALUES (NULL, ?)";
		preparedStatement = (PreparedStatement) connect.prepareStatement(query);

		preparedStatement.setString(1, name);
		preparedStatement.executeUpdate();
		boolean b = isClientExist(name);
		if (b)
			System.out.println("User added successfully");
		return b;

	}

	public void insert(String name, String montant, String duree, String taux, String annuite) throws Exception {
		DBConnect();

		close();
	}

	public int login(String name, String password) throws Exception {
		DBConnect();
		query = "select * from TClient where NomCli=? and password=?";
		preparedStatement = (PreparedStatement) connect.prepareStatement(query);
		preparedStatement.setString(1, name);
		preparedStatement.setString(2, password);
		resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {
			int id = resultSet.getInt("NumCli");
			close();
			return id;
		}
		close();
		return 0;
	}

	// You need to close the resultSet
	private void close() {
		try {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (connect != null) {
				connect.close();
			}
		} catch (Exception e) {

		}
	}
}
