<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.mysql.jdbc.Driver"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	session = request.getSession(false);
	if (session.getAttribute("Name") == null) {
		if (request.getParameter("Name") == null) {
			response.sendRedirect("index.jsp");
		} else {
			String name = request.getParameter("Name");
			String password = request.getParameter("Password");

			Connection connect = null;
			PreparedStatement preparedStatement;
			ResultSet resultSet;
			String query;

			int id = 0;

			try {
				// This will load the MySQL driver, each DB has its own driver
				Class.forName("com.mysql.jdbc.Driver");
				// Setup the connection with the DB

			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("Loading class mysql jdbc: " + e);
			}

			try {
				connect = DriverManager.getConnection("jdbc:mysql://localhost/sia_projet1?", "anass", "123456");
				query = "select * from TClient where NomCli=? and password=?";
				preparedStatement = connect.prepareStatement(query);
				preparedStatement.setString(1, name);
				preparedStatement.setString(2, password);
				resultSet = preparedStatement.executeQuery();
				if (resultSet.next()) {
					id = resultSet.getInt("NumCli");
				}
			} catch (Exception e) {
				System.out.println("Connect to database: " + e);
			}
			int login = id;
			if (login > 0) {
				session.setAttribute("Id", login);
				session.setAttribute("Name", name);
			} else {
				response.sendRedirect("index.jsp?error=true");
			}
		}
	}
%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Web Services Project</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">


<!--  <link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"> -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<style>
.slidecontainer {
	width: 100%;
}

.slider {
	-webkit-appearance: none;
	width: 100%;
	height: 25px;
	background: #d3d3d3;
	outline: none;
	opacity: 0.7;
	-webkit-transition: .2s;
	transition: opacity .2s;
}

.slider:hover {
	opacity: 1;
}

.slider::-webkit-slider-thumb {
	-webkit-appearance: none;
	appearance: none;
	width: 25px;
	height: 25px;
	background: #286090;
	cursor: pointer;
}

.slider::-moz-range-thumb {
	width: 25px;
	height: 25px;
	background: #286090;
	cursor: pointer;
}

section {
	margin-bottom: 10%;
}
</style>

<!--  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script> -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>

<!--<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
</head>
<body>
	<header>

		<!--<nav class="navbar bg-primary ">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="#"> <img src="safebox.svg"
						width="30" height="30" class="d-inline-block align-top" alt="" />
					</a>
				</div>
			</div>
		</nav>-->
		<!-- Navigation -->
		<nav class="navbar navbar-light bg-light justify-content-between">
			<a class="navbar-brand"><img src="safebox.svg" width="40"
				height="40" class="d-inline-block align-center" alt="" />IAO Bank</a>
			<h5 class="text-center">
				Welcome
				<%=session.getAttribute("Name")%></h5>
			<form class="form-inline" action="logout.jsp">

				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Autre
					client</button>
			</form>
		</nav>
	</header>
	<section>

		<div class="container">

			<h2>Web Services Project</h2>


			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item"><a class="nav-link active" id="home-tab"
					data-toggle="tab" href="#annuite" role="tab" aria-controls="home"
					aria-selected="true">Annuite</a></li>
				<li class="nav-item"><a class="nav-link" id="profile-tab"
					data-toggle="tab" href="#capital" role="tab"
					aria-controls="profile" aria-selected="false">Capital</a></li>
				<li class="nav-item"><a class="nav-link" id="contact-tab"
					data-toggle="tab" href="#duree" role="tab" aria-controls="contact"
					aria-selected="false">Duree</a></li>
			</ul>
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="annuite" role="tabpanel"
					aria-labelledby="home-tab">

					<h3>Annuite</h3>
					<form id="myForm" method="post" class="needs-validation">
						<div class="form-group has-danger">
							<label for="capitalInput">Capital: (en DH) </label> <input
								required="required" name="capital" type="number" min="0"
								class="form-control" id="capitalInput"
								aria-describedby="emailHelp" placeholder="Inserer Capital">
							<div class="invalid-feedback">Please fill out this field.</div>
						</div>
						<div class="form-group">
							<label for="tauxInput">Taux: (en %)</label> <input type="number"
								required="required" min="0" step="0.01" class="form-control"
								id="tauxInput" placeholder="Inserer Taux">
							<div class="invalid-feedback">Please fill out this field.</div>
						</div>


						<div class="form-group slidecontainer">
							<label for="dureeInput">Duree: <span id="demo"></span>
								(mois)
							</label> <input required="required" type="range" min="12" max="100"
								value="50" class="slider" id="dureeInput">
						</div>

						<button type="button" class="sauvgarder btn btn-primary">Calculer
							et Sauvgarder</button>
					</form>

				</div>
				<div class="tab-pane fade" id="capital" role="tabpanel"
					aria-labelledby="profile-tab">...</div>
				<div class="tab-pane fade" id="duree" role="tabpanel"
					aria-labelledby="contact-tab">...</div>
			</div>
		</div>
	</section>

	<div id="dataModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3 class="text-center modal-title">Annuite</h3>
				</div>
				<div class="modal-body" id="employee_detail"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>

	<script>
		$(document)
				.ready(
						function() {

							$('.sauvgarder')
									.click(
											function() {
												console.log($('#dureeInput')
														.val());
												if ($('#capitalInput').val().length != 0
														&& $('#tauxInput')
																.val().length != 0
														&& $('#dureeInput')
																.val().length != 0) {
													var myKeyVals = {
														"capital" : $(
																'#capitalInput')
																.val(),
														"taux" : $('#tauxInput')
																.val(),
														"duree" : $(
																'#dureeInput')
																.val()
													};

													$
															.ajax({
																url : "http://localhost:8080/SOAPClient/aide.jsp",
																method : "post",
																dataType : "json",
																data : myKeyVals,
																success : function(
																		data) {
																	$(
																			'.modal-body')
																			.html(
																					data._return);
																	$(
																			'#exampleModal')
																			.modal(
																					"show");
																}
															});
												} else {
													window
															.alert("All inputs are required !!");
												}

											});
						});

		var slider = document.getElementById("dureeInput");
		var output = document.getElementById("demo");
		output.innerHTML = slider.value;

		slider.oninput = function() {
			output.innerHTML = this.value;
		}
	</script>

</body>
</html>
