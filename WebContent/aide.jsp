<%@page import="java.time.LocalDate"%>
<%@page import="java.sql.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.*"%>
<%@page import="sia.anass.CreditStub.Annuite"%>
<%@page import="sia.anass.CreditStub.AnnuiteResponse"%>
<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.rmi.RemoteException"%>
<%@page import="org.apache.axis2.AxisFault"%>
<%@page import="sia.anass.CreditStub"%>
<%
	AnnuiteResponse res = null;
	String id = session.getAttribute("Id").toString();
	int capital = Integer.valueOf(request.getParameter("capital")),
			duree = Integer.valueOf(request.getParameter("duree"));

	double taux = Double.valueOf(request.getParameter("taux"));

	try {
		CreditStub stub = new CreditStub();

		Annuite annuite = new Annuite();
		annuite.setCapital(capital);
		annuite.setDuree(duree);
		annuite.setTaux(taux);

		res = stub.annuite(annuite);

	} catch (AxisFault e) {
		e.printStackTrace();
	} catch (RemoteException e) {
		e.printStackTrace();
	}
	String s = "{\"_return\":\"" + res.get_return().toString() + "\"}";

	Connection connect = null;
	PreparedStatement preparedStatement;
	ResultSet resultSet;
	String query;

	try {
		// This will load the MySQL driver, each DB has its own driver
		Class.forName("com.mysql.jdbc.Driver");
		// Setup the connection with the DB

	} catch (Exception e) {
		// TODO: handle exception
		System.out.println("Loading class mysql jdbc: " + e);
	}

	try {
		connect = DriverManager.getConnection("jdbc:mysql://localhost/sia_projet1?", "anass", "123456");
		query = "INSERT INTO `TCredit` (`NumCre`, `DatCre`, `MonCre`, `DurCre`, `TauCre`, `AnnCre`, `DatPreVer`, `NumCli`) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)";
		
		preparedStatement = connect.prepareStatement(query);
		preparedStatement.setDate(1, Date.valueOf(LocalDate.now()));
		preparedStatement.setString(2, String.valueOf(capital));
		preparedStatement.setString(3, String.valueOf(duree));
		preparedStatement.setString(4, String.valueOf(taux));
		preparedStatement.setString(5, res.get_return().toString());
		preparedStatement.setString(6, String.valueOf("idk"));
		preparedStatement.setString(7, id);
		preparedStatement.executeUpdate();

	} catch (Exception e) {
		System.out.println("Connect to database: " + e);
	}
%>
<%=s%>




